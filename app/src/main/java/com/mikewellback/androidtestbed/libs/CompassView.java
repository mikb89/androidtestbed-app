package com.mikewellback.androidtestbed.libs;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikewellback.androidtestbed.R;

public class CompassView extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compass_view);

        com.redinput.compassview.CompassView compass = (com.redinput.compassview.CompassView) findViewById(R.id.compass);

        compass.setDegrees(57);
        compass.setBackgroundColor(Color.YELLOW);
        compass.setLineColor(Color.RED);
        compass.setShowMarker(false);
        compass.setRangeDegrees(270);

        compass.setOnCompassDragListener(new com.redinput.compassview.CompassView.OnCompassDragListener() {
            @Override
            public void onCompassDragListener(float degrees) {
                // Do what you want with the degrees
            }
        });
    }

}
