package com.mikewellback.androidtestbed.libs;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.daimajia.easing.BaseEasingMethod;
import com.daimajia.easing.Glider;
import com.daimajia.easing.Skill;
import com.mikewellback.androidtestbed.R;
import com.mikewellback.androidtestbed.libs.classes.DrawView;

public class AnimationEasingFunctions extends Activity {

    private ListView mEasingList;
    private EasingAdapter mAdapter;
    private View mTarget;

    private DrawView mHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation_easing_functions);
        mEasingList = (ListView) findViewById(R.id.easing_list);
        mAdapter = new EasingAdapter(this);
        mEasingList.setAdapter(mAdapter);
        mTarget = findViewById(R.id.target);
        mHistory = (DrawView) findViewById(R.id.history);
        mEasingList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mHistory.clear();
                Skill s = (Skill) view.getTag();
                AnimatorSet set = new AnimatorSet();
                mTarget.setTranslationX(0);
                mTarget.setTranslationY(0);
                set.playTogether(
                        Glider.glide(s, 1200, ObjectAnimator.ofFloat(mTarget, "translationY", 0, dipToPixels(AnimationEasingFunctions.this, -(160 - 3))), new BaseEasingMethod.EasingListener() {
                            @Override
                            public void on(float time, float value, float start, float end, float duration) {
                                mHistory.drawPoint(time, duration, value - dipToPixels(AnimationEasingFunctions.this, 60));
                            }
                        })
                );
                set.setDuration(1200);
                set.start();
            }
        });

    }

    public static float dipToPixels(Context context, float dipValue) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics);
    }

    public class EasingAdapter extends BaseAdapter {

        private Context mContext;
        public EasingAdapter(Context context) {
            mContext = context;
        }

        @Override
        public int getCount() {
            return Skill.values().length;
        }

        @Override
        public Object getItem(int i) {
            return Skill.values()[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Object o = getItem(i);
            BaseEasingMethod b = ((Skill)o).getMethod(1000);
            int start = b.getClass().getName().lastIndexOf(".") + 1;
            String name = b.getClass().getName().substring(start);
            View v = LayoutInflater.from(mContext).inflate(android.R.layout.simple_list_item_1,null);
            TextView tv = (TextView)v.findViewById(android.R.id.text1);
            tv.setText(name);
            v.setTag(o);
            return v;
        }
    }
}

