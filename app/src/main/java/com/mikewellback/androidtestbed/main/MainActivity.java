package com.mikewellback.androidtestbed.main;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mikewellback.androidtestbed.libs.AndroidViewAnimations;
import com.mikewellback.androidtestbed.libs.AnimationEasingFunctions;
import com.mikewellback.androidtestbed.R;
import com.mikewellback.androidtestbed.libs.AndroidAsync;
import com.mikewellback.androidtestbed.libs.AndroidSlidingUpPanel;
import com.mikewellback.androidtestbed.libs.AndroidSwipeLayout;
import com.mikewellback.androidtestbed.libs.CompassView;
import com.mikewellback.androidtestbed.libs.Compass_View;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    RecyclerView list_rec;
    Adapter adapter = new Adapter();
    LinearLayoutManager layoutManager;
    ArrayList<Object[]> data = new ArrayList<Object[]>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list_rec = (RecyclerView) findViewById(R.id.list_rec);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        list_rec.setLayoutManager(layoutManager);
        list_rec.setItemAnimator(new DefaultItemAnimator());
        list_rec.setAdapter(adapter);

        addLibs("Web",
                lib("AndroidAsync", AndroidAsync.class, "koush"));
        addLibs("UI",
                lib("AndroidSlidingUpPanel", AndroidSlidingUpPanel.class, "umano"),
                lib("AndroidSwipeLayout", AndroidSwipeLayout.class, "daimajia"));
        addLibs("Animations",
                lib("AndroidViewAnimations", AndroidViewAnimations.class, "daimajia"),
                lib("AnimationEasingFunctions", AnimationEasingFunctions.class, "daimajia"));
        addLibs("Widgets",
                lib("CompassView", CompassView.class, "RedInput"),
                lib("Compass-View", Compass_View.class, "arbelkilani"));

        adapter.notifyDataSetChanged();
    }

    public String gh(String author, String lib) {
        return "https://github.com/" + author + "/" + lib;
    }

    public Object[] lib(String lib, Class activity, String author) {
        return new Object[]{R.layout.item_library, lib, activity, author};
    }

    public void addLibs(String category, Object[]... libs) {
        if (category != null && category.length() > 0 && libs != null) {
            data.add(new Object[]{R.layout.item_category, category});
            data.addAll(Arrays.asList(libs));
        }
    }

    public class Adapter extends RecyclerView.Adapter<Holder> {

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int layout) {
            return new Holder(getLayoutInflater().inflate(layout, parent, false));
        }

        @Override
        public int getItemViewType(int position) {
            return (int) data.get(position)[0];
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            Object[] item = data.get(position);
            switch (getItemViewType(position)) {
                case R.layout.item_category:
                    holder.name_txt.setText((String) item[1]);
                    break;
                case R.layout.item_library:
                    holder.name_txt.setText((String) item[1]);
                    holder.wrap_lay.setTag(item[2]);
                    holder.wrap_lay.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(view.getContext(), (Class<?>) view.getTag()));
                        }
                    });
                    break;
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    class Holder extends RecyclerView.ViewHolder {
        FrameLayout wrap_lay;
        TextView name_txt;
        Holder(@NonNull View itemView) {
            super(itemView);
            wrap_lay = itemView.findViewById(R.id.wrap_lay);
            name_txt = itemView.findViewById(R.id.name_txt);
        }
    }
}
